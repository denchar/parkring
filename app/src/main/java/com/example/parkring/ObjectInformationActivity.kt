package com.example.parkring

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_object_information.*
import kotlinx.android.synthetic.main.content_object_information.*

class ObjectInformationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_object_information)
        setSupportActionBar(toolbar)
        val currentId = intent.getIntExtra("ID", 0)
        when (currentId) {
            0 -> {
                title_tv.text = getString(R.string.zakhaim_gates)
                description_tv.text = getString(R.string.zakhaim_gates_description)
                Picasso.get().load(R.drawable.zakhaimskie_gates).into(image_iv)
            }
            1 -> {
                title_tv.text = getString(R.string.reduit_bastiona)
                description_tv.text = getString(R.string.reduit_bastiona_description)
                Picasso.get().load(R.drawable.reduit_bastiona_img).into(image_iv)
            }
            2 -> {
                title_tv.text = getString(R.string.johanes_house)
                description_tv.text = getString(R.string.johanes_house_description)
                Picasso.get().load(R.drawable.johanes_house_img).into(image_iv)
            }
        }
    }


    companion object {
        val OBJECT_ID = "OBJECT_ID"
    }
}


