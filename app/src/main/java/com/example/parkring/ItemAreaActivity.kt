package com.example.parkring

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolygonOptions

class ItemAreaActivity : AppCompatActivity(), OnMapReadyCallback {

    var placesList = listOf<Place>()


    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_area)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        placesList = listOf(
            Place(
                0,
                R.drawable.zakhaimskie_gates,
                getString(R.string.zakhaim_gates),
                getString(R.string.zakhaim_gates_description),
                54.709597,
                20.538258
            ), Place(
                1,
                R.drawable.reduit_bastiona_img,
                getString(R.string.reduit_bastiona),
                getString(R.string.reduit_bastiona_description),
                54.709097,
                20.5376718
            ), Place(
                2,
                R.drawable.johanes_house_img,
                getString(R.string.johanes_house),
                getString(R.string.johanes_house_description),
                54.7066192,
                20.5379092
            )

        )
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val itemCoord: ArrayList<LatLng> = intent.getParcelableArrayListExtra(AllParkAreaActivity.BOUNDS)
        val itemOptions = PolygonOptions()
        addCoord(itemOptions, itemCoord)

        val itemPolygon = mMap.addPolygon(itemOptions)
        itemPolygon.isClickable = true

        val point = LatLng(54.709896, 20.540341)
        val cameraPosition = CameraPosition.Builder()
            .target(point)
            .zoom(15f)
            .build()
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        addPoints(mMap, placesList)

        mMap.setOnMarkerClickListener {
            Log.v("Listener", "Маркер кликнули: ${it.id}")
            val intent = Intent(this,ObjectInformationActivity::class.java)
            when(it.id){
                "m0" -> intent.putExtra("ID",0)
                "m1" -> intent.putExtra("ID",1)
                "m2" -> intent.putExtra("ID",2)
            }
            startActivity(intent)
            true
        }
    }

    fun addPoints(map: GoogleMap, points: List<Place>) {
        for (currentPoint in points) {
            map.addMarker(
                MarkerOptions().position(
                    LatLng(
                        currentPoint.firstCoord,
                        currentPoint.secondCoord
                    )
                ).title(currentPoint.title)
            )
        }
    }


    fun addCoord(polygonOptions: PolygonOptions, list: List<LatLng>) {
        for (item in list) {
            polygonOptions.add(item)
        }
    }
}
