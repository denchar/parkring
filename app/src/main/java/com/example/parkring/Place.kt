package com.example.parkring

data class Place(
    val id: Int,
    val imageId: Int,
    val title: String,
    val description: String,
    val firstCoord:Double,
    val secondCoord:Double
)