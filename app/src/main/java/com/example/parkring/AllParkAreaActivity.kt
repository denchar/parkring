package com.example.parkring


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolygonOptions


class AllParkAreaActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    companion object {
        var ID = "id"
        var NAME = "name"
        var BOUNDS = "bounds"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_park_area)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    fun addCoord(polygonOptions: PolygonOptions, list: List<LatLng>) {
        for (item in list) {
            polygonOptions.add(item)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val zakhaimCoord = ArrayList<LatLng>()
        zakhaimCoord.addAll(
            listOf(
                LatLng(54.713990, 20.537199),
                LatLng(54.712798, 20.539718),
                LatLng(54.710396, 20.539263),
                LatLng(54.709627, 20.539621),
                LatLng(54.709583, 20.541713),
                LatLng(54.709112, 20.542173),
                LatLng(54.709067, 20.543142),
                LatLng(54.707225, 20.544801),
                LatLng(54.707092, 20.542606),

                LatLng(54.705487, 20.542063),
                LatLng(54.706460, 20.537686),
                LatLng(54.709646, 20.538008),
                LatLng(54.713328, 20.535991)
            )
        )

        val zakhaimOptions = PolygonOptions().fillColor(Color.GREEN)
        addCoord(zakhaimOptions, zakhaimCoord)

        val zakhaimPolygon = mMap.addPolygon(zakhaimOptions)
        zakhaimPolygon.isClickable = true


        val zakhaim = AreaModel("zakhaim", 0, zakhaimCoord)



        mMap.setOnPolygonClickListener { polygon ->
            if (polygon == zakhaimPolygon) {
                val intent = Intent(applicationContext, ItemAreaActivity::class.java)
                intent.putExtra(AllParkAreaActivity.NAME, zakhaim.name)
                intent.putExtra(AllParkAreaActivity.NAME, zakhaim.id)
                intent.putParcelableArrayListExtra(AllParkAreaActivity.BOUNDS, zakhaim.polygon)
                startActivity(intent)
            }
        }

        val point = LatLng(54.709896, 20.540341)
        val cameraPosition = CameraPosition.Builder()

            .target(point)
            .zoom(15f)
            .build()

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))


    }

}
